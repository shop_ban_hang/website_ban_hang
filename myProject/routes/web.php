<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController\DashboardController;
use App\Http\Controllers\AdminController\ProfileController;
use App\Http\Controllers\AdminController\CountryController;
use App\Http\Controllers\AdminController\BlogController;
use App\Http\Controllers\MemberController\RegisterController;
use App\Http\Controllers\MemberController\LoginCotroller;
use App\Http\Controllers\MemberController\BlogUserController;
use App\Http\Controllers\MemberController\AccountController;
use App\Http\Controllers\AdminController\CategoryController;
use App\Http\Controllers\AdminController\BrandController;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\MemberController\ProductdetailController;
use App\Http\Controllers\MemberController\CartController;
use App\Http\Controllers\MemberController\CheckoutController;
use App\Http\Controllers\MemberController\SearchController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/addCountry',function(){
    return view('admin/country/addCountry');
});
Route::get('/add_category',function(){
    return view('admin/category/add_category');
});
Route::get('/add_brand',function(){
    return view('admin/brand/add_brand');
});
Route::get('addblog',function(){
    return view('admin/blog/addblog');
});


Auth::routes();


Route::group(['middleware'=>'admin'], function(){
    Route::group(['prefix'=>'admin'],function(){
        
        Route::get('/dashboard',[DashboardController::class,'index']);
         //Profile
        Route::get('/profile',[ProfileController::class,'index']);
        Route::post('/profile',[ProfileController::class,'update']);

            //Country
        Route::get('/country',[CountryController::class,'index']);
        // Route::get('/addCountry',[CountryController::class,'showcreate']);
        Route::post('/addCountry',[CountryController::class,'create']);
        Route::get('editCountry/{id}',[CountryController::class,'edit']);
        Route::post('editCountry/{id}',[CountryController::class,'update']);
        Route::get('deleteCountry/{id}',[CountryController::class,'destroy']);
            //Catagory
        Route::get('/category',[CategoryController::class,'index']);
        Route::post('/add_category',[CategoryController::class,'create']);
        Route::get('edit_category/{id}',[CategoryController::class,'edit']);
        Route::post('edit_category/{id}',[CategoryController::class,'update']);
        Route::get('delete_category/{id}',[CategoryController::class,'destroy']);
            //Brand 
        Route::get('/brand',[BrandController::class,'index']);
        Route::get('/add_brand',[BrandController::class,'showcreate']);
        Route::post('/add_brand',[BrandController::class,'create'])->name('add_brand1');
        Route::get('edit_brand/{id}',[BrandController::class,'edit']);
        Route::post('edit_brand/{id}',[BrandController::class,'update']);
            //Blog
        Route::get('blog',[BlogController::class,'index']);
        Route::post('addblog',[BlogController::class,'create']);
        Route::get('editblog/{id}',[BlogController::class,'edit']);
        Route::post('editblog/{id}',[BlogController::class,'update']);
        Route::get('deleteblog/{id}',[BlogController::class,'destroy']);

    });

});



   



//Fontend 
Route::group(['namespace'=>'MemberController'],function(){
    Route::get('/home1',[IndexController::class,'index']);

    Route::group(['middleware'=>['membernotlogin']],function(){
        Route::get('/registerFE',[RegisterController::class,'index']);
        Route::post('/registerFE',[RegisterController::class,'create']);
        Route::get('/loginFE',[LoginCotroller::class,'show'])->name('loginFE');
        Route::post('/loginFE',[LoginCotroller::class,'login']);
    });
    Route::get('/logout',[LoginCotroller::class,'logout'])->name('logoutFE');

            //BlogFE
    Route::get('/blogFE',[BlogUserController::class,'show']);
    Route::get('/blogdetail/{id}',[BlogUserController::class,'showdetail']);
    Route::post('/rateblog',[BlogUserController::class,'rateblog'])->name('rateblog');
    Route::get('/cmtuser',[BlogUserController::class,'cmtuser'])->name('cmtuser');
    Route::post('/cmtblog',[BlogUserController::class,'cmtblog'])->name('cmtblog');
    Route::get('/cmtblog/{id}',[BlogUserController::class,'cmtnho']);
    
        //Account FE
    Route::get('account',[AccountController::class,'index']);
    Route::post('account',[AccountController::class,'update'])->name('account');
    Route::get('account/my_product',[AccountController::class,'showproduct']);
    Route::get('account/add_product',[AccountController::class,'trangadd']);
    Route::post('add_product',[AccountController::class,'addproduct'])->name('add_product');
    Route::get('account/edit_product/{id}',[AccountController::class,'edit_product']);
    Route::post('account/edit_product/{id}',[AccountController::class,'update_product'])->name('update_product');
    Route::get('account/delete_product/{id}',[AccountController::class,'destroy']);
        //Dentail Product
    Route::get('product',[ProductdetailController::class,'show']);
    Route::get('product_detail/{id}',[ProductdetailController::class,'index']);

        //Cart
    Route::post('/addCart',[CartController::class,'addCart'])->name('addCart');
    Route::get('/cart',[CartController::class,'index']);
    Route::post('/upCart',[CartController::class,'upCart'])->name('updateCart');
    Route::post('/downCart',[CartController::class,'Downcart'])->name('Downcart');
    Route::post('/deleteCart',[CartController::class,'DeleteCart'])->name('DeleteCart');
        //Checkout
    Route::get('/checkout',[CheckoutController::class,'index']);
    Route::post('/sendmail',[CheckoutController::class,'testMail'])->name('postmail');

        //search
    Route::get('/search_product',[SearchController::class,'index']);
    Route::post('/search',[SearchController::class,'gettext'])->name('search_product');
    Route::post('/search_all',[SearchController::class,'get_search'])->name('search_product_all');
    Route::post('/search_price',[SearchController::class,'search_price'])->name('search_price');
});



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                