<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\TestapiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});



    Route::get('/product',[ProductController::class, 'productHome']);
    Route::post('/product',[ProductController::class, 'store']);
    Route::get('/testapi',[TestapiController::class,"index"]);
    // Route::post('testapi','TestApiController@store');
    // Route::get('testapi/{id}', 'TestApiController@show');
    // Route::put('testapi/{id}','TestApiController@update');
    // Route::delete('testapi/{id}', 'TestApiController@destroy');