<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('rateblog', function (Blueprint $table) {
            $table->id(); // Trường id tự động tăng
            $table->unsignedBigInteger('id_user'); // Trường id của người dùng
            $table->unsignedBigInteger('id_blog'); // Trường id của bài viết
            $table->integer('rate'); // Trường rate (số điểm)
            $table->timestamps(); // Trường thời gian tạo và cập nhật

            // Khóa ngoại đến bảng người dùng và bài viết
            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_blog')->references('id_blog')->on('blog');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('rateblog');
    }
};
