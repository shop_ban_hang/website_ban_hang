<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id('id_product');
            $table->unsignedBigInteger('id_user');
            $table->string('name');
            $table->float('price');
            $table->unsignedBigInteger('id_category');
            $table->unsignedBigInteger('id_brand');
            $table->unsignedInteger('status')->default(0)->comment='0:new 1:sale';
            $table->string('sale');
            $table->string('company');
            $table->string('image');
            $table->string('detail');
            $table->timestamps();
            $table->foreign('id_user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};
