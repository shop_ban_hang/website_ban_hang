@extends('fontend.layoutFE.share2')
@section('content')
<style type="text/css">
	.input_name{
		width: 60px;
	}
	.select_form{
		width: 120px;
	}
</style>
<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Search Items</h2>
						<form class="menu" method="post" action="{{route('search_product_all')}}">
							@csrf
							<input class="input_name" type="text" name="name" placeholder="Name"/>
							<select class="select_form" name="price">
									<option value="">Choose price</option>
									<option value="0-100">0-100</option>
									<option value="100-500">100-500</option>
									<option value="500-1000">500-1000</option>
							</select>
							<select class="select_form" name="category">
									<option value="">Choose Category</option>
								<?php foreach ($dataCategory as $value): ?>	
									<option value="{{$value['id_category']}}">{{$value['name_category']}}</option>
								<?php endforeach ?>
							</select>
							<select class="select_form" name="brand">
								<option value="">Choose Brand</option>
								<?php foreach ($dataBrand as $value): ?>	
									<option value="{{$value['id_brand']}}">{{$value['name_brand']}}</option>
								<?php endforeach ?>
							</select>
							<select class="select_form" name="status" id="status">
								<option value="0">NEW</option>
								<option value="1">SALE</option>
							</select>
							<button type="submit">Search</button>
						</form>
						<div class="display_product">
							<?php if (isset($data)): ?>
							<?php foreach ($data as $value): 
								$img = json_decode($value['image'])
							 ?>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('fontend/images/product-details/'.$img[0])}}" alt="" />
												<h2>${{$value['price']}}</h2>
												<p>{{$value['name']}}</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											<div class="product-overlay">
												<div class="overlay-content">
													<h2>${{$value['price']}}</h2>
													<p>{{$value['name']}}</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
											</div>
										</div>
										<div class="choose">
											<ul class="nav nav-pills nav-justified">
												<li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
												<li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
											</ul>
										</div>
									</div>
								</div>
							<?php endforeach ?>
						<?php endif ?>

						<?php if (isset($xx)): ?>
							<?php foreach ($xx as $value): 
								$img = json_decode($value['image'])
							 ?>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('fontend/images/product-details/'.$img[0])}}" alt="" />
												<h2>${{$value['price']}}</h2>
												<p>{{$value['name']}}</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											<div class="product-overlay">
												<div class="overlay-content">
													<h2>${{$value['price']}}</h2>
													<p>{{$value['name']}}</p>
													<a id="{{$value['id_product']}}" href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
											</div>
										</div>
										<div class="choose">
											<ul class="nav nav-pills nav-justified">
												<li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
												<li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
											</ul>
										</div>
									</div>
								</div>
							<?php endforeach ?>
						<?php endif ?>
						</div>
						
					</div><!--features_items-->
				</div>
<script type="text/javascript">
		$(document).ready(function() {
		$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});	
		$(".price").slider().on("slideStop", function () {
				var priceChange = $('.tooltip-inner').text();
				var html = "";
				// console.log(typeof priceChange);
				$.ajax({
					type :'POST',
					url:"{{route('search_price')}}",
					data:{
						priceChange : priceChange,
					},
					success:function(data){
						var dataProduct =data.success;
						
						if (dataProduct.length>0) {
							$('.display_product').empty();
							// console.log(dataProduct);
							dataProduct.map(function(product){
								console.log(product.id_product);
								var img = JSON.parse(product.image);
								const myArr =img[0] ;
								
							html += '<div class="col-sm-4">'+
									'<div class="product-image-wrapper">'+
										'<div class="single-products">'+
											'<div class="productinfo text-center">'+
												'<img src="fontend/images/product-details/'+myArr+'" alt="" />'+
												'<h2>'+'$'+product.price+'</h2>'+
												'<p>'+product.name+'</p>'+
												'<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>'+
											'</div>'+
											'<div class="product-overlay">'+
												'<div class="overlay-content">'+
													'<h2>'+product.price+'</h2>'+
													'<p>'+product.name+'</p>'+
													'<a id="'+product.id_product+'" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>AAdd to cart</a>'+
												'</div>'+
											'</div>'+
										'</div>'+
										'<div class="choose">'+
											'<ul class="nav nav-pills nav-justified">'+
												'<li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>'+
												'<li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>'+
											'</ul>'+
										'</div>'+
									'</div>'+
								'</div>'
						});
						}
						
						$(".display_product").append(html);
						$('a.add-to-cart').click(function() {
							var id = $(this).attr('id');
							$.ajax({
								type : 'POST',
								url : "{{route('addCart')}}",
								data:{
									id : id,
								},
								success:function(data){
									$('#addcart').text('SL : '+data.success111);
									alert('Thêm sản phẩm thành công');
								}
							});
						});
						
					}
				})
			})		
		})
		$('a.add-to-cart').click(function() {
				var id = $(this).attr('id');
				alert("aaaaa");
				$.ajax({
					type : 'POST',
					url : "{{route('addCart')}}",
					data:{
						id : id,
					},
					success:function(data){
						$('#addcart').text('SL : '+data.success111);
						alert('Thêm sản phẩm thành công');
					}
				});
			});

</script>
@endsection
