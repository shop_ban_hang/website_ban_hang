@extends('fontend.layoutFE.share')
@section('content')
	<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Features Items</h2>

						<?php foreach($dataProduct as $product): 
							$arrayImg = json_decode($product['image']);
						?>
						<div class="col-sm-4">
							<div class="product-image-wrapper">
								<div class="single-products">
										<div class="productinfo text-center">
											<img src="{{asset('fontend/images/product-details/'.$arrayImg[0])}}" alt="" />
											<h2>{{$product['price']}}$</h2>
											<p>{{$product['company']}}</p>
											<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
										</div>
										<div class="product-overlay">
											<div class="overlay-content">

												<h2>{{$product['price']}}$</h2>
												<p>{{$product['company']}}</p>
												<a id="{{$product['id_product']}}"  class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to carttttt</a>
												<a href="{{ url('product_detail/'.$product['id_product'])}}" class=" btn btn-default "></i>Details Product</a>
											</div>
										</div>
								</div>
								<div class="choose">
									<ul class="nav nav-pills nav-justified">
										<li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
										<li><a href="#"><i class="fa fa-plus-square"></i>Add to compare</a></li>
									</ul>
								</div>
							</div>
						</div>
						<?php endforeach; ?>
						
					</div><!--features_items-->
					
					
					
	</div><!--/recommended_items-->
	<script type="text/javascript">
		$(document).ready(function() {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});				
			$('a.add-to-cart').click(function() {
				var id = $(this).attr('id');
				// alert(id);
				$.ajax({
					type : 'POST',
					url : "{{route('addCart')}}",
					data:{
						id : id,
					},
					success:function(data){
						$('#addcart').text('SL : '+data.success111);
						alert('Thêm sản phẩm thành công');
					}
				});
			});
		});
	</script>
@endsection