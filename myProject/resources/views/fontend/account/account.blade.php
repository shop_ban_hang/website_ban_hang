@extends('fontend.layoutFE.share1')
@section('content')
<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Account</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">account</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="account/my_product">My product</a></h4>
								</div>
							</div>
							
						</div><!--/category-products-->
					
						
					</div>
				</div>
				<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">Update user</h2>
						 <div class="signup-form"><!--sign up form-->
						<h2>Update User</h2>
						<form class="account_user" method="post"  enctype="multipart/form-data">
							@csrf
							<label>Name</label>
							<input type="text" name="name" value="{{$dataUser['name']}}">
							<label>Email</label>
							<input type="email" name="mail" value="{{$dataUser['email']}}">
							<label>Password</label>
							<input type="password" name="password" value=""placeholder="Password">
							<label>Address</label>
							<input type="text" name="adress" value="{{$dataUser['adress']}}">
							<label>Phone</label>
							<input type="text" name="phone" value="{{$dataUser['phone']}}">
							<label>Avatar</label>
							<input type="file" name="avatar">
							<button type="submit">UPDATE</button>
							@if(session('success'))
					            <h5 style="color: red; width: 100%; text-align:center;"> {{ session('success') }}</h5>
					        @endif
				</form>
					</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
@endsection
