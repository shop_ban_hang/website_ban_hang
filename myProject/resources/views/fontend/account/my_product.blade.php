@extends('fontend.layoutFE.share1')
@section('content')
		<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Account</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="../account">account</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="my_product">My product</a></h4>
								</div>
							</div>
							
						</div><!--/category-products-->
					
						
					</div>
				</div>
				<div class="col-sm-9">
					<div class="table-responsive cart_info">
						<h3>MY PRODUCT</h3>
						<table class="table table-condensed">
							<thead>
								<tr class="cart_menu">
									<td class="id">ID</td>
									<td class="name">NAME</td>
									<td class="image">IMAGE</td>
									<td class="price">Price</td>
									<td class="actions">Action</td>
								</tr>
							</thead>
							<tbody>

								<?php foreach ($dataProduct as $value) :
									$arrayImage = json_decode($value['image'],true);
									// var_dump ($arrayImage);
								 ?>
									<tr>


										<td>{{$value['id_product']}}</td>
										<td>{{$value['name']}}</td>
										<td><img style="width: 100px ; height: 100px" src="{{asset('fontend/images/product-details/'.$arrayImage[0])}}"></td>
										<td>{{$value['price']}}</td>
										<td><a href="{{ url('account/edit_product/'.$value['id_product'])}}">Edit</a>
											<a href="{{url('account/delete_product/'.$value['id_product'])}}">Delete</a>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<button><a href="add_product">Add Product</a></button>
					</div>
				</div>
			</div>
		</div>
	</section>
         @if(session('success'))
			<h3 style="color: red; width: 100%; text-align:center;"> {{ session('success') }}</h3>
		 @endif      

	
@endsection