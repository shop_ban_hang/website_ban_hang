@extends('fontend.layoutFE.share1')
@section('content')
	<div class="container">
			<div class="row">
				<div class="col-sm-3">
					<div class="left-sidebar">
						<h2>Account</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							
							
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="../account">account</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="my_product">My product</a></h4>
								</div>
							</div>
							
						</div><!--/category-products-->
					
						
					</div>
				</div>
				<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">ADD PRODUCT</h2>
						 <div class="signup-form"><!--sign up form-->
						<form action="{{route('add_product')}}" method="post" enctype="multipart/form-data">
							@csrf
							<label>Name Product</label>
							<input type="text" name="name" placeholder="Name"/>
							<label>Price</label>
							<input type="text" name="price" placeholder="Price"/>
							<label>Category</label>
							<select name="category">
								<?php foreach($dataCategory as $category): ?>
									<option value="{{$category['id_category']}}">{{$category['name_category']}}</option>
								<?php endforeach; ?>
							</select>
							<label>Brand</label>
							<select name="brand">
								<?php foreach($dataBrand as $brand): ?>
									<option value="{{$brand['id_brand']}}">{{$brand['name_brand']}}</option>
								<?php endforeach; ?>
							</select>
							<label>Satatus</label>
							<select name="status" id="status">
								<option value="0">NEW</option>
								<option value="1">SALE</option>
							</select>
							<div class="phantram" style="display: none;">
								<label>% Sale</label>
								<input type="text" name="phamtram">
							</div>
							<label>Profile</label>
							<input type="text" placeholder="Company Profile" name="company" />
							<input type="file" name="image[]" multiple />
							<label>Detail</label>
							<textarea name="detail"></textarea>						
							<button type="submit" class="btn btn-default">ADD</button>
						</form>
					@if(session('success'))
						<h3 style="color: red; width: 100%; text-align:center;"> {{ session('success') }}</h3>
					@endif
					</div>
					</div>
				</div>
			</div>
		</div>
	<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
        $('#status').change(function() {
            if ($(this).val() ==1) {
                $('.phantram').show();
            } else {
                $('.phantram').hide();
            }
        });
    });
	</script>
	 
@endsection