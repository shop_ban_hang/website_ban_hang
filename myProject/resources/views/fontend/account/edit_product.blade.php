@extends('fontend.layoutFE.share1')
@section('content')
	<div class="container">
			<div class="row">
				
				<div class="col-sm-9">
					<div class="blog-post-area">
						<h2 class="title text-center">EDIT PRODUCT</h2>
						 <div class="signup-form"><!--sign up form-->
						<form action="{{route('update_product',['id' => $dataEdit['id_product']])}}" method="post" enctype="multipart/form-data">
							@csrf
							<label>Name Product</label>
							<input type="text" name="name" placeholder="Name" value="{{$dataEdit['name']}}" />
							<label>Price</label>
							<input type="text" name="price" placeholder="Price" value="{{$dataEdit['price']}}"/>
							<label>Category</label>
							<select name="category">
								<?php foreach($dataCategory as $category): ?>
									<option value="{{$category['id_category']}}" <?php if ($category['id_category']==$dataEdit['id_category']): ?>
										selected
									<?php endif ?>>{{$category['name_category']}}</option>
								<?php endforeach; ?>
							</select>
							<label>Brand</label>
							<select name="brand">
								<option value="1">GUCCI</option>
								<option value="2">DIOR</option>
								<option value="3">CHANNEL</option>
							</select>
							<label>Satatus</label>
							<select name="status" id="status" value="1">
								<option value="0">NEW</option>
								<option value="1">SALE</option>
							</select>
							<div class="phantram" style="display: none;">
								<label>% Sale</label>
								<input type="text" name="phamtram" value="{{$dataEdit['sale']}}"/>
							</div>
							<label>Profile</label>
							<input type="text" placeholder="Company Profile" name="company" value="{{$dataEdit['company']}}"/>
							<label>Image</label>
							<input type="file" name="image[]" multiple />
							<label><h4>Chon hinh muon xoa</h4></label>
							<?php 
								$arrayImage = json_decode($dataEdit['image'],true);
									
								foreach ($arrayImage as $img) {
							?><div style="display: inline-grid;">
						            <img src="{{asset('fontend/images/product-details/85x84'.$img)}}">
						            <input type="checkbox" name="imagesToDelete[]" value="{{ $img }}">
						        </div>

							<?php
								}
							?>
							<br>
							<label>Detail</label>
							<textarea name="detail">{{$dataEdit['detail']}}</textarea>						
							<button type="submit" class="btn btn-default">Update</button>
						</form>
						 @if(session('error'))
				                   <h5 style="color: red; width: 100%; text-align:center;"> {{ session('error') }}</h2>
						@endif
					
					</div>
					</div>
				</div>
			</div>
		</div>
		<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
	        $('#status').change(function() {
	            if ($(this).val() ==1) {
	                $('.phantram').show();
	            } else {
	                $('.phantram').hide();
	            }
	        });
	    });
		</script>
@endsection