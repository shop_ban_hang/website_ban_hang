@extends('fontend.layoutFE.share1')
@section('content')

	<div class="blog-post-area">
		<h2 class="title text-center">Latest From our Blog</h2>
		<?php foreach ($dataBlog as  $value) {
		?>
			<div class="single-blog-post">
				<h3>{{$value['title']}}</h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i> Mac Doe</li>
									<li><i class="fa fa-clock-o"></i>{{ date_format(date_create($value['created_at']), " H:m") }}</li>
									<li><i class="fa fa-calendar"></i> {{ date_format(date_create($value['created_at']), "d/m/Y") }}</li>
								</ul>
								<span>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star"></i>
										<i class="fa fa-star-half-o"></i>
								</span>
							</div>
							<a href="">
								<img style="width: 500px; height: 500px" src="{{ asset('admin/upload/blog/' . $value['image']) }}" alt="">
							</a>
							<p>{{$value['description']}}</p>
							<a style="margin-bottom: 10px;" class="btn btn-primary" href="{{ url('blogdetail/'.$value['id_blog'])}}">Read More</a>
			</div>
		<?php
		} ?>	
		{{ $dataBlog->links('pagination::bootstrap-4')}}
@endsection