@extends('fontend.layoutFE.share1')
@section('content')
	
	
	<div class="blog-post-area">
						<h2 class="title text-center">Latest From our Blog</h2>
						<div class="single-blog-post">
							<h3>{{$dataDetail['title']}}</h3>
							<div class="post-meta">
								<ul>
									<li><i class="fa fa-user"></i></li>
									<li><i class="fa fa-clock-o"></i> 1:33 pm</li>
									<li><i class="fa fa-calendar"></i> DEC 5, 2013</li>
								</ul>
								
							</div>
							<a href="">
								<img style="width:500px; height:500px" src="{{ asset('admin/upload/blog/' . $dataDetail['image']) }}" alt="">
							</a><br>
							{{$dataDetail['content']}}

							<div class="pager-area">
								<ul class="pager pull-right">
									<li><a href="#">Pre</a></li>
									<li><a href="#">Next</a></li>
								</ul>
							</div>
						</div>

			            <div class="rate">
			                <div class="vote">

			                	<?php 
			                		for ($i=1; $i <=5 ; $i++) { 
			                	?>
			                		<div class="star_1 ratings_stars  <?php echo $i <= $TBC ? 'ratings_over' :" " 	?>"><input value="{{$i}}" type="hidden"></div>
			                	<?php
			                		}
			                	 ?>			                		
			                    <span class="rate-np" style="font-size:15px">{{round($TBC)}}</span>

			                </div> 
			            </div>
			            
			           <div class="response-area">
				           	<div class="comment">
				            	COMMENT
				            	<form class =frmcmt action="{{route('cmtblog')}}" method="post">
				            		@csrf
				            		<textarea name="ndcomment" class="ndcomment"></textarea>
				            		<input type="hidden" class="id_blog" name="id_blog" value="{{$dataDetail['id_blog']}}">
				            		<input type="hidden" class="levelcmt" name="level" value="0">
				            		<button type="submit" class="btncmt">SEND</button>
				            	</form>
				            	 @if(session('success'))
				                   <h5 style="color: red; width: 100%; text-align:center;"> {{ session('success') }}</h2>
				           		 @endif
				            </div>

							<h2>COMMENT	</h2>
							<ul class="media-list">
							<?php foreach ($dataCmt as $value): ?>
							    <?php if ($value['level'] == 0): ?>
							        <li class="media">    
							            <a class="pull-left" href="#">    
							                <img style="width: 50px; height: 50px" class="media-object" src="{{ asset('avatar/' . $value['avatar']) }}" alt="">
							            </a>
							            <div class="media-body">
							                <ul class="sinlge-post-meta">
							                    <li><i class="fa fa-user"></i><?php echo $value['name']; ?></li>
							                    <li><i class="fa fa-clock-o"></i>{{ date_format(date_create($value['created_at']), "d/m/Y H:m") }}</li>
							                </ul>
							                <p><?php echo $value['comment']; ?></p>
							                <a id="<?php echo $value['id_comment']; ?>" class="btn btn-primary" href="#"><i class="fa fa-reply"></i>Replay</a>
							            </div>
							        </li>

							        <?php foreach ($dataCmt as $childComment): ?>
							            <?php if ($childComment['level'] == $value['id_comment']): ?>
							                <li class="media second-media">
												<a class="pull-left" href="#">
													<img style="width: 50px; height: 50px" class="media-object" src="{{ asset('avatar/' . $childComment['avatar']) }}" alt="">
												</a>
												<div class="media-body">
													<ul class="sinlge-post-meta">
														<li><i class="fa fa-user"></i>{{$childComment['name']}}</li>
														<li><i class="fa fa-clock-o"></i>{{ date_format(date_create($childComment['created_at']), "H:m") }}</li>
														<li><i class="fa fa-calendar"></i> {{ date_format(date_create($childComment['created_at']), "d/m/Y") }}</li>
													</ul>
													<p>{{$childComment['comment']}}</p>
												</div>
											</li>
							            <?php endif; ?>
							        <?php endforeach; ?>
							    <?php endif; ?>
							<?php endforeach; ?>

							</ul>					
					</div><!--/Response-area-->
			          

		</div><!--/blog-post-area-->
					<script type="text/javascript">
						$(document).ready(function(){
							$.ajaxSetup({
							    headers: {
							        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							    }
							});
							//vote
							$('.ratings_stars').hover(
					            // Handles the mouseover
					            function() {
					                $(this).prevAll().andSelf().addClass('ratings_hover');
					                // $(this).nextAll().removeClass('ratings_vote'); 
					            },
					            function() {
					                $(this).prevAll().andSelf().removeClass('ratings_hover');
					                // set_votes($(this).parent());
					            }
					        );

							$('.ratings_stars').click(function(){
								var checkLogin = '{{Auth::check()}}'
								if (checkLogin) {
									var Values =  $(this).find("input").val();
							        // alert(Values);
							    	if ($(this).hasClass('ratings_over')) {
							            $('.ratings_stars').removeClass('ratings_over');
							            $(this).prevAll().andSelf().addClass('ratings_over');
							        } else {
							        	$(this).prevAll().andSelf().addClass('ratings_over');

							        	// 

							        	$.ajax({
											type : 'POST',
											url : "{{route('rateblog')}}",
											data:{
												Values : Values,
												id_blog : {{$dataDetail['id_blog']}},
											},
											success:function(data){
												console.log(data.success);
											}
										});
							        }
								}else{
									alert('chua Login');
								}					
						    });
						   $(".frmcmt").submit(function(){
						    	var checkcmt = $("textarea.ndcomment").val();
						    	// alert(checkcmt);
						    	var checkLogin = '{{Auth::check()}}'
						    	if (checkLogin) {
						    		if (checkcmt=="") {
						    			alert('chưa cmt')
						    		}
						    		else{
						    			return true;
						    		}
						    	}
						    	else{
						    		alert('chua login')
						    	}
						    	return false;
						    });

						   $('.btn-primary').click(function() {
						   		var idcmt = $(this).attr('id');	
						   		$("input.levelcmt").val(idcmt);
						   		
						   })
						});
					</script>	    
@endsection