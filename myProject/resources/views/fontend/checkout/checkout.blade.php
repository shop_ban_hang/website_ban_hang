@extends('fontend.layoutFE.share1')
@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div><!--/breadcrums-->

			<div class="step-one">
				<h2 class="heading">Step1</h2>
			</div>
			<div class="checkout-options">
				<h3>New User</h3>
				<p>Checkout options</p>
				<ul class="nav">
					<li>
						<label><input type="checkbox"> Register Account</label>
					</li>
					<li>
						<label><input type="checkbox"> Guest Checkout</label>
					</li>
					<li>
						<a href=""><i class="fa fa-times"></i>Cancel</a>
					</li>
				</ul>
			</div><!--/checkout-options-->

			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
					<div class="col-sm-3">
						<div class="shopper-info">
							<p>Shopper Information</p>
							<form class="formlogin" action="{{route('postmail')}}" method="post">
								 @csrf
								<input type="text" class="formloginhide" name="name" placeholder="Name"/>
								<input type="email"class="formloginhide"  name="email" placeholder="Email Address"/>
								<input type="password"class="formloginhide" name="password" placeholder="Password"/>
								<button type="submit" class="btn"><a class="btn btn-primary" >Continue</a></button>
							</form>
								
						</div>
					</div>
					<div class="col-sm-5 clearfix">
						<div class="bill-to">
							<p>Bill To</p>
							<div class="form-one">
								<form >
									<input type="text" placeholder="Company Name">
									<input type="text" placeholder="Email*">
									<input type="text" placeholder="Title">
									<input type="text" placeholder="First Name *">
									<input type="text" placeholder="Middle Name">
									<input type="text" placeholder="Last Name *">
									<input type="text" placeholder="Address 1 *">
									<input type="text" placeholder="Address 2">
								</form>
							</div>
							<div class="form-two">
								<form >
									<input type="text" placeholder="Zip / Postal Code *">
									<select>
										<option>-- Country --</option>
										<option>United States</option>
										<option>Bangladesh</option>
										<option>UK</option>
										<option>India</option>
										<option>Pakistan</option>
										<option>Ucrane</option>
										<option>Canada</option>
										<option>Dubai</option>
									</select>
									<select>
										<option>-- State / Province / Region --</option>
										<option>United States</option>
										<option>Bangladesh</option>
										<option>UK</option>
										<option>India</option>
										<option>Pakistan</option>
										<option>Ucrane</option>
										<option>Canada</option>
										<option>Dubai</option>
									</select>
									<input type="password" placeholder="Confirm password">
									<input type="text" placeholder="Phone *">
									<input type="text" placeholder="Mobile Phone">
									<input type="text" placeholder="Fax">
								</form>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="order-message">
							<p>Shipping Order</p>
							<textarea name="message"  placeholder="Notes about your order, Special Notes for Delivery" rows="16"></textarea>
							<label><input type="checkbox"> Shipping to bill address</label>
						</div>	
					</div>					
				</div>
			</div>
			<div class="review-payment">
				<h2>Review & Payment</h2>
			</div>

			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php if (session()->has('cart')){
							$dataCart = session()->get('cart');
							foreach ($dataCart as $key => $value) {
						?>
							<tr>
							<td class="cart_product">
								<a href=""><img src="{{asset('fontend/images/product-details/85x84'.$dataCart[$key]['img'])}}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$dataCart[$key]['name']}}</a></h4>
								<p>Web ID:{{$dataCart[$key]['id_product']}}</p>
							</td>
							<td class="cart_price">
								<p>${{$dataCart[$key]['price']}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href="#"> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="{{$dataCart[$key]['qty']}}" autocomplete="off" size="2">
									<a class="cart_quantity_down" href="#"> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">${{$dataCart[$key]['price']*$dataCart[$key]['qty']}}</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td>
						</tr>
						<?php
							}
						} ?>
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>$59</td>
									</tr>
									<tr>
										<td>Exo Tax</td>
										<td>$2</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td><span>$61</span></td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="payment-options">
					<span>
						<label><input type="checkbox"> Direct Bank Transfer</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
					</span>
				</div>
		</div>
	</section> <!--/#cart_items-->
	<script type="text/javascript">
		$(document).ready(function(){
			var checkLogin = "{{Auth::check()}}";
			if (checkLogin) {
				$('.formlogin').hide();
			}
			else{
				$('.formlogin').show();
			}
		})
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
			var sumCart=0;
			$('.cart_quantity_up').click(function() {
				var id=$(this).closest('tr').find('.cart_description p').text().replace('Web ID:',"");
				var price =parseFloat($(this).closest('tr').find('.cart_price p').text().replace('$',""));
				var qty =$(this).closest('.cart_quantity_button').find('.cart_quantity_input').val();
				var total=$(this).closest('tr').find('.cart_total p');
				// alert(total);
				qty++;
				// alert(qty);
				$(this).closest('.cart_quantity_button').find('.cart_quantity_input').val(qty);	
				$.ajax({
					type :'POST',
					url:"{{route('updateCart')}}",
					data:{
						idUp : id,
					},
					success:function(data){
						$('.sumtotal .total').text('$'+data.total);
						$('.total .total').text('$'+(data.total+2));

					}
				})
				var newtotal = price*qty;
				total.text('$'+newtotal);
			});
			$('.cart_quantity_down').click(function() {
				var id=$(this).closest('tr').find('.cart_description p').text().replace('Web ID:',"");
				var price =parseFloat($(this).closest('tr').find('.cart_price p').text().replace('$',""));
				var qty =$(this).closest('.cart_quantity_button').find('.cart_quantity_input').val();
				var total=$(this).closest('tr').find('.cart_total p');
				// alert(total);
				qty--;
				// alert(qty);
				$(this).closest('.cart_quantity_button').find('.cart_quantity_input').val(qty);	
				if (qty<1) {
					$(this).closest('tr').remove();
				}
				$.ajax({
					type :'POST',
					url:"{{route('Downcart')}}",
					data:{
						idDown : id,
					},
					success:function(data){
						$('.sumtotal .total').text('$'+data.total);
						 // console.log(data.success);
					}
				})
				var newtotal = price*qty;
				total.text('$'+newtotal);

			});
			$('.cart_quantity_delete').click(function() {
				var id=$(this).closest('tr').find('.cart_description p').text().replace('Web ID:',"");
				$(this).closest('tr').remove();	
				$.ajax({
					type :'POST',
					url:"{{route('DeleteCart')}}",
					data:{
						idRemove : id,
					},
					success:function(data){
						$('.sumtotal .total').text('$'+data.total);
					}
				})

			});


		});
	</script>
@endsection