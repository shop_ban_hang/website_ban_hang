<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
</head>
<body>
	<h3>Hi ^^ {{$name}} đơn hàng của bạn đã được đặt thành công </h3>
	<a href="">Nhấn vào đây để xác nhận đơn hàng</a>
	<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php if (session()->has('cart')){
							$dataCart = session()->get('cart');
							foreach ($dataCart as $key => $value) {
						?>
							<tr>
							<td class="cart_product">
								<a href=""><img src="{{asset('fontend/images/product-details/85x84'.$dataCart[$key]['img'])}}" alt=""></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$dataCart[$key]['name']}}</a></h4>
								<p>Web ID:{{$dataCart[$key]['id_product']}}</p>
							</td>
							<td class="cart_price">
								<p>${{$dataCart[$key]['price']}}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<a class="cart_quantity_up" href="#"> + </a>
									<input class="cart_quantity_input" type="text" name="quantity" value="{{$dataCart[$key]['qty']}}" autocomplete="off" size="2">
									<a class="cart_quantity_down" href="#"> - </a>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">${{$dataCart[$key]['price']*$dataCart[$key]['qty']}}</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href=""><i class="fa fa-times"></i></a>
							</td>
						</tr>
						<?php
							}
						} ?>
						<tr>
							<td colspan="4">&nbsp;</td>
							<td colspan="2">
								<table class="table table-condensed total-result">
									<tr>
										<td>Cart Sub Total</td>
										<td>$59</td>
									</tr>
									<tr>
										<td>Exo Tax</td>
										<td>$2</td>
									</tr>
									<tr class="shipping-cost">
										<td>Shipping Cost</td>
										<td>Free</td>										
									</tr>
									<tr>
										<td>Total</td>
										<td><span>$61</span></td>
									</tr>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
</body>
</html>