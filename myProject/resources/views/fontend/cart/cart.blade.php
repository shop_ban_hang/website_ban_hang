@extends('fontend.layoutFE.share1')
@section('content')
	<section id="cart_items">
		<div class="container">
			<div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="#">Home</a></li>
				  <li class="active">Shopping Cart</li>
				</ol>
			</div>
			<div class="table-responsive cart_info">
				<table class="table table-condensed">
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
						<?php
						  $dataCart = session()->get('cart');
						  if (session()->has('cart')) {
							foreach ($dataCart as $key => $value) {
						?>
							<tr>
								<td class="cart_product">
									<a href=""><img src="{{asset('fontend/images/product-details/85x84'.$dataCart[$key]['img'])}}" alt=""></a>
								</td>
								<td class="cart_description">
									<h4><a href="">{{$dataCart[$key]['name']}}</a></h4>
									<p>Web ID:{{$dataCart[$key]['id_product']}}</p>
								</td>
								<td class="cart_price">
									<p>${{$dataCart[$key]['price']}}</p>
								</td>
								<td class="cart_quantity">
									<div class="cart_quantity_button">
										<a class="cart_quantity_up" href="#"> + </a>
										<input class="cart_quantity_input" type="text" name="quantity" value="{{$dataCart[$key]['qty']}}" autocomplete="off" size="2">
										<a class="cart_quantity_down" href="#"> - </a>
									</div>
								</td>
								<td class="cart_total">
									<p class="cart_total_price">${{$dataCart[$key]['price']*$dataCart[$key]['qty']}}</p>
								</td>
								<td class="cart_delete">
									<a class="cart_quantity_delete" href="#"><i class="fa fa-times"></i></a>
								</td>
							</tr>
						<?php
							  }
							}
						?>
						<?php if (!session()->has('cart')): ?>
							<h1>Chưa có sản phẩm nào</h1>
						<?php endif ?>
						
					</tbody>
				</table>
			</div>
		</div>
	</section> <!--/#cart_items-->
	<section id="do_action">
		<div class="container">
			<div class="heading">
				<h3>What would you like to do next?</h3>
				<p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="chose_area">
						<ul class="user_option">
							<li>
								<input type="checkbox">
								<label>Use Coupon Code</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Use Gift Voucher</label>
							</li>
							<li>
								<input type="checkbox">
								<label>Estimate Shipping & Taxes</label>
							</li>
						</ul>
						<ul class="user_info">
							<li class="single_field">
								<label>Country:</label>
								<select>
									<option>United States</option>
									<option>Bangladesh</option>
									<option>UK</option>
									<option>India</option>
									<option>Pakistan</option>
									<option>Ucrane</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
								
							</li>
							<li class="single_field">
								<label>Region / State:</label>
								<select>
									<option>Select</option>
									<option>Dhaka</option>
									<option>London</option>
									<option>Dillih</option>
									<option>Lahore</option>
									<option>Alaska</option>
									<option>Canada</option>
									<option>Dubai</option>
								</select>
							
							</li>
							<li class="single_field zip-field">
								<label>Zip Code:</label>
								<input type="text">
							</li>
						</ul>
						<a class="btn btn-default update" href="">Get Quotes</a>
						<a class="btn btn-default check_out" href="">Continue</a>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="total_area">
						<ul>
							<li class="sumtotal">Cart Sub Total <span class="total">${{session()->get('total')}}</span></li>
							<li>Eco Tax <span>$2</span></li>
							<li>Shipping Cost <span>Free</span></li>
							<li class="total">Total <span class="total">${{session()->get('total')+2}}</span></li>

						</ul>
							<a class="btn btn-default update" href="">Update</a>
							<a class="btn btn-default check_out" href="checkout">Check Out</a>
					</div>
				</div>
			</div>
		</div>
	</section><!--/#do_action-->
	<script type="text/javascript">
		$(document).ready(function() {
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
				});
			var sumCart=0;
			$('.cart_quantity_up').click(function() {
				var id=$(this).closest('tr').find('.cart_description p').text().replace('Web ID:',"");
				var price =parseFloat($(this).closest('tr').find('.cart_price p').text().replace('$',""));
				var qty =$(this).closest('.cart_quantity_button').find('.cart_quantity_input').val();
				var total=$(this).closest('tr').find('.cart_total p');
				// alert(total);
				qty++;
				// alert(qty);
				$(this).closest('.cart_quantity_button').find('.cart_quantity_input').val(qty);	
				$.ajax({
					type :'POST',
					url:"{{route('updateCart')}}",
					data:{
						idUp : id,
					},
					success:function(data){
						$('.sumtotal .total').text('$'+data.total);
						$('.total .total').text('$'+(data.total+2));

					}
				})
				var newtotal = price*qty;
				total.text('$'+newtotal);
			});
			$('.cart_quantity_down').click(function() {
				var id=$(this).closest('tr').find('.cart_description p').text().replace('Web ID:',"");
				var price =parseFloat($(this).closest('tr').find('.cart_price p').text().replace('$',""));
				var qty =$(this).closest('.cart_quantity_button').find('.cart_quantity_input').val();
				var total=$(this).closest('tr').find('.cart_total p');
				// alert(total);
				qty--;
				// alert(qty);
				$(this).closest('.cart_quantity_button').find('.cart_quantity_input').val(qty);	
				if (qty<1) {
					$(this).closest('tr').remove();
				}
				$.ajax({
					type :'POST',
					url:"{{route('Downcart')}}",
					data:{
						idDown : id,
					},
					success:function(data){
						$('.sumtotal .total').text('$'+data.total);
						 // console.log(data.success);
					}
				})
				var newtotal = price*qty;
				total.text('$'+newtotal);

			});
			$('.cart_quantity_delete').click(function() {
				var id=$(this).closest('tr').find('.cart_description p').text().replace('Web ID:',"");
				$(this).closest('tr').remove();	
				$.ajax({
					type :'POST',
					url:"{{route('DeleteCart')}}",
					data:{
						idRemove : id,
					},
					success:function(data){
						$('.sumtotal .total').text('$'+data.total);
					}
				})

			});


		});
	</script>
@endsection