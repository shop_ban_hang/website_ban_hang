@extends('fontend.layoutFE.share2')
@section('content')
<style type="text/css">
	input{
		width: 40px;
	}
	select{
		width: 100px;
	}
</style>
<div class="col-sm-9 padding-right">
					<div class="features_items"><!--features_items-->
						<h2 class="title text-center">Search Items</h2>
						<form class="menu" method="post" action="{{route('search_product_all')}}">
							@csrf
							<input type="text" name="name" placeholder="Name"/>
							<select name="price">
									<option value="">Choose price</option>
									<option value="0-100">0-100</option>
									<option value="100-500">100-500</option>
									<option value="500-1000">500-1000</option>
							</select>
							<select name="category">
									<option value="">Choose Category</option>
								<?php foreach ($dataCategory as $value): ?>	
									<option value="{{$value['id_category']}}">{{$value['name_category']}}</option>
								<?php endforeach ?>
							</select>
							<select name="brand">
								<option value="">Choose Brand</option>
								<?php foreach ($dataBrand as $value): ?>	
									<option value="{{$value['id_brand']}}">{{$value['name_brand']}}</option>
								<?php endforeach ?>
							</select>
							<select name="status" id="status">
								<option value="0">NEW</option>
								<option value="1">SALE</option>
							</select>
							<button type="submit">Search</button>
						</form>
							<?php foreach ($data as  $value): 
								$img = json_decode($value['image'])
							?>
								<div class="col-sm-4">
									<div class="product-image-wrapper">
										<div class="single-products">
											<div class="productinfo text-center">
												<img src="{{asset('fontend/images/product-details/'.$img[0])}}" alt="" />
												<h2>${{$value['price']}}</h2>
												<p>{{$value['name']}}</p>
												<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
											</div>
											<div class="product-overlay">
												<div class="overlay-content">
													<h2>${{$value['price']}}</h2>
													<p>{{$value['name']}}</p>
													<a href="#" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</a>
												</div>
											</div>
										</div>
										<div class="choose">
											<ul class="nav nav-pills nav-justified">
												<li><a href=""><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
												<li><a href=""><i class="fa fa-plus-square"></i>Add to compare</a></li>
											</ul>
										</div>
									</div>
								</div>
							<?php endforeach ?>

						
					</div><!--features_items-->
				</div>
@endsection