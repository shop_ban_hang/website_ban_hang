@extends('admin.layout.share')
@section('content')
<style type="text/css">
            table{
                width: 800px;
                margin: auto;
                text-align: center;
            }
            tr {
                border: 1px solid;
            }
            th {
                border: 1px solid;
            }
            td {
                border: 1px solid;
            }
            h1{
                text-align: center;
                color: red;
            }
            #button{
                margin: 2px;
                margin-right: 10px;
                float: right;
            }
        </style>
		 <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">AddBlog</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Country</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="form-group col-md-12">
                <form method="post"  enctype="multipart/form-data">
                    @csrf
                    <label style="font-weight: 500;">Title</label>
                    <input type="text" name="title"> <br>
                    <label style="font-weight: 500;">Image</label>
                    <input type="file" name="image" ><br>
                    <label style="font-weight: 500;">Descrpition</label>
                    <input type="text" name="description"> <br> <br>
                    <textarea name="content" class="form-control" id="textArea"></textarea>
                    <button type="submit" name="btnAddBlog">Create</button>
                </form>
            </div>
           
            <footer class="footer text-center">
                All Rights Reserved by Nice admin. Designed and Developed by
                <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
            @if(session('success'))
                <div class="alert alert-success">
                   <h2 style="color: red; width: 100%; text-align:center;"> {{ session('success') }}</h2>
                </div>
            @endif
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
@endsection