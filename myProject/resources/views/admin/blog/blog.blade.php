@extends('admin.layout.share')
@section('content')

		 <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">BLOG</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">BLOG</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Title</th>
                                                <th scope="col">Image</th>
                                                <th scope="col">Descrpition</th>
                                                <th scope="col">Content</th>
                                                <th scope="col">Edit</th>
                                                <th scope="col">Delete</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        	<?php foreach ($data as  $value) {
                                        	?>
                                        	<tr>
                                                <th scope="row">{{$value['id_blog']}}</th>
                                                <td>{{$value["title"]}}</td>
                                                <td>{{$value["image"]}}</td>
                                                <td>{{$value["description"]}}</td>
                                                <td>{{$value["content"]}}</td>
                                                <td><a href="{{ url('editblog/'.$value['id_blog'])}}">Edit</a></td>
                                                <td><a href="{{ url('deleteblog/'.$value['id_blog'])}}">Delete</a></td>
                                            </tr>
                                        	<?php	
                                        	} ?>
                                        </tbody>
                                    </table>
                                    <button ><a href="addblog" style="color: red; font-weight: 500;">Create Blog</a></button>
            </div>
            
            <footer class="footer text-center">
                All Rights Reserved by Nice admin. Designed and Developed by
                <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
            @if(session('success'))
                <div class="alert alert-success">
                   <h2 style="color: red; width: 100%; text-align:center;"> {{ session('success') }}</h2>
                </div>
            @endif
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
@endsection