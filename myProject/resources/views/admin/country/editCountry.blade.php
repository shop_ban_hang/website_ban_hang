@extends('admin/layout.share')
@section('content')
<style type="text/css">
            table{
                width: 800px;
                margin: auto;
                text-align: center;
            }
            tr {
                border: 1px solid;
            }
            th {
                border: 1px solid;
            }
            td {
                border: 1px solid;
            }
            h1{
                text-align: center;
                color: red;
            }
            #button{
                margin: 2px;
                margin-right: 10px;
                float: right;
            }
        </style>
         <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Add Country</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Country</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
                <?php 
                    foreach ($dataName as $value) {
                        $name = $value['name'];
                        $id = $value['id_country'];
                    };
                 ?>
                <form method="post">
                        @csrf
                    <label style="color: red;">Name Country</label><br>
                    <input type="text" name="name" value="<?php echo $name ?>">
                    <button style="background: red;" type="submit">Update</button>
                </form>
            </div>
            
              
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
@endsection