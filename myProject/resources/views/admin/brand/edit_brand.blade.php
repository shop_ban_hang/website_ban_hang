@extends('admin/layout.share')
@section('content')

<style type="text/css">
            
            table{
                width: 800px;
                margin: auto;
                text-align: center;
            }
            tr {
                border: 1px solid;
            }
            th {
                border: 1px solid;
            }
            td {
                border: 1px solid;
            }
            h1{
                text-align: center;
                color: red;
            }
            #button{
                margin: 2px;
                margin-right: 10px;
                float: right;
            }
</style>
         <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Add Brand</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Brand</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
               
                <form method="post">
                        @csrf
                    <label style="color: red;">Edit Brand</label><br>
                    <input type="text" name="name_brand" value="{{$editbrand['name_brand']}}">
                    <button style="background: red;" type="submit">Update</button>
                </form>
            </div>
            
              
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
@endsection