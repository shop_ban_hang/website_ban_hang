@extends('admin.layout.share')
@section('content')
    <style type="text/css">
            table{
                width: 800px;
                margin: auto;
                text-align: center;
            }
            tr {
                border: 1px solid;
            }
            th {
                border: 1px solid;
            }
            td {
                border: 1px solid;
            }
            h1{
                text-align: center;
                color: red;
            }
            #button{
                margin: 2px;
                margin-right: 10px;
                float: right;
            }
    </style>

		 <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Brand</h4>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex align-items-center justify-content-end">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item">
                                        <a href="#">Home</a>
                                    </li>
                                    <li class="breadcrumb-item active" aria-current="page">Brand</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
           
            <table id="datatable" style="border: 1px solid">

            
            <thead>
                <tr role="row">
                    <th>ID</th>
                    <th>Name</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
               
            </thead>
            <tbody>
	      		<?php foreach($data as $value): ?>
                    <tr>
                        <td>{{$value['id_brand']}}</td>
                        <td>{{$value['name_brand']}}</td>
                        <td><a href="{{ url('edit_brand/'.$value['id_brand'])}}">Edit</a></td>
                        <td><a href="{{ url('delete_brand/'.$value['id_brand'])}}">Delete</a></td>
                    </tr>
                   <?php endforeach; ?> 
             
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="8">
                            <a href="{{url('/add_brand')}}"><button id="button">Add</button></a><br>  
                    </td>
                </tr>
            </tfoot>


        </table>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
           
            <footer class="footer text-center">
                All Rights Reserved by Nice admin. Designed and Developed by
                <a href="https://wrappixel.com">WrapPixel</a>.
            </footer>
            @if(session('success'))
                <div class="alert alert-success">
                   <h2 style="color: red; width: 100%; text-align:center;"> {{ session('success') }}</h2>
                </div>
            @endif
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
@endsection