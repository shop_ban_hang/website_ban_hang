<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AdminModel\BrandModel;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = BrandModel::all()->toArray();
        return view('admin/brand/brand',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        BrandModel::insert([
            'name_brand'=>$request->name_brand,
        ]);
        return redirect('brand')->with('success', 'Thêm thành công!');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function showcreate(Request $request)
    {
        return view('admin/brand/add_brand');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $editbrand = BrandModel::where('id_brand',$id)->get()->toArray();
        // dd($editcategory);
        $editbrand = $editbrand[0];
        return view('admin/brand/edit_brand',compact('editbrand'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        BrandModel::where('id_brand',$id)->update([
            'name_brand'=>$request->name_brand,
        ]);
        return redirect('brand')->with('success', 'Edit thành công!');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
