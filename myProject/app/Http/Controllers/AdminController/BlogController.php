<?php

namespace App\Http\Controllers\AdminController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AdminModel\BlogModel;
use Illuminate\Support\Facades\Auth;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = BlogModel::all()->toArray();
        // dd($data);
        return view('admin/blog/blog',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
{   
    $file = $request->image;
    $filename = $file->getClientOriginalName();
    $file->move('admin/upload/blog',$filename);
    $data = [
        'title' => $request->title,
        'image'=>$filename,
        'description' => $request->description,
        'content' => $request->content
    ];

    BlogModel::insert($data);

    return redirect()->back()->with('success', 'Blog post created successfully.');
}

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $dataBlog = BlogModel::where('id_blog',$id)->get()->toArray();
        // dd($dataBlog);
        return view('admin/blog/editblog', compact('dataBlog'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {    
        BlogModel::where('id_blog',$id)->update([
            'title' => $request->title,
            'description' => $request->description,
            'content' => $request->content
        ]);
        $file = $request->image;
        if (!empty($file)) {
            $filename = $file->getClientOriginalName();
            $file->move('admin/upload/blog',$filename);
             BlogModel::where('id_blog',$id)->update([
                'image'=>$filename,
             ]);
        }
        return redirect('blog')->with('success', 'Update thanh cong');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
           BlogModel::Where('id_blog',$id)->delete();
            return redirect('blog')->with('success', 'Xoa thanh cong !');
    }
}
