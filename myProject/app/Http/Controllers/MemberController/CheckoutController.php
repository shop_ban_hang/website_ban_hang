<?php

namespace App\Http\Controllers\MemberController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Mail;
use Illuminate\Support\Facades\Auth; 
use App\Models\User;

class CheckoutController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('fontend/checkout/checkout');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function testMail(Request $request)
    {  
            
        if (Auth::check()) {
            $emailget = Auth::user()->email;
            $name = Auth::user()->name;
        }
        else{
           User::insert([
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=>bcrypt($request->password),
                'level'=>0,
            ]);
           $emailget = $request->email;
           $name=$request->name; 
        }
        $data = session()->get('cart');
          Mail::send('fontend/checkout/sendmail',compact('data','name'),function($email) use($emailget){
                $email->subject('Demo test mail');
                $email->to($emailget);
        });
        return view('fontend/checkout/successMail',compact('emailget'));
       
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
