<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\UserModel\ProductsModel;
use Illuminate\Support\Facades\Auth;
use Validator;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public $successStatus = 200;
    public function productHome(){
        $getProductHome = ProductsModel::orderBy('id_product')->limit(6)->get()->toArray();
        return response()->json([
            'response' => 'success',
            'data' => $getProductHome,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required|numeric',
            'category' => 'required',
            'brand' => 'required',
            'status' => 'required',
            'phamtram' => 'required',
            'company' => 'required',
            'image.*' => 'image|mimes:jpeg,png,jpg,gif|max:2048', // Validate each image
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        if ($request->hasfile('image')) {
            $data = [];

            foreach ($request->file('image') as $image) {
                $name = $image->getClientOriginalName();
                $name_2 = "85x84" . $image->getClientOriginalName();
                $name_3 = "329x380" . $image->getClientOriginalName();

                // Lưu ảnh
                $path = public_path('fontend/images/product-details/' . $name);
                $path_2 = public_path('fontend/images/product-details/' . $name_2);
                $path_3 = public_path('fontend/images/product-details/' . $name_3);

                Image::make($image->getRealPath())->save($path);
                Image::make($image->getRealPath())->resize(85, 84)->save($path_2);
                Image::make($image->getRealPath())->resize(392, 380)->save($path_3);

                $data[] = $name;
            }

            if (count($data) > 3) {
                return response()->json(['error' => 'Chỉ chọn được tối đa 3 hình ảnh'], 400);
            } else {
                $id_user = Auth::id();
                $product = ProductsModel::create([
                    'id_user' => $id_user,
                    'name' => $request->name,
                    'price' => $request->price,
                    'id_category' => $request->input('category'),
                    'id_brand' => $request->input('brand'),
                    'status' => $request->input('status'),
                    'sale' => $request->phamtram,
                    'company' => $request->company,
                    'image' => json_encode($data),
                    'detail' => $request->detail,
                ]);
                return response()->json(['success' => 'Thêm sản phẩm thành công', 'product' => $product], 201);
            }
        } else {
            return response()->json(['error' => 'Chưa chọn hình ảnh'], 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     */

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
